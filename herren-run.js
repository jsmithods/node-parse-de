var request = require("request"),
    cheerio = require("cheerio"),
    fs = require("fs"),
    url = "https://www.outdoorshop.de/Bekleidung/Herren/",
    PRRRRURL = "https://www.outdoorshop.de/Bekleidung/Damen/Fleece-Wolle/Victory-LS-Zip-Wmn.html";
const util = require('util');

// function to download content
var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){

    request(uri).pipe(fs.createWriteStream(`./images/${filename}`)).on('close', callback);
  });
};


    let ArrayOfProducts = [];

    // request multiple products page
    request(url, function (error, response, body) {
    if (!error) {
        let $ = cheerio.load(body),
            products = $(".productData");
            for (let i = 0; i < products.length - 1; i++) {
                // console.log(util.inspect(products[0], false, null))

                // let productObject = document.getElementById("productList");
                // let productObjectS = document.getElementsByClassName("productData");

                let productUrl = products[i].children[1].attribs.href;
                let productCategory = products[i].children[3].children[1].children[1].children[0].data;
                let productName = products[i].children[3].children[1].children[3].children[0].data;
                let productPrice = products[i].children[3].children[3].children[1].data.slice(0, -3);
                let productMainIMG = products[i].children[1].children[1].children[1].attribs.src;

                let PreparedObjectToPush = {
                    productUrl, productCategory, productName, productPrice, productMainIMG
                };


                ArrayOfProducts.push(PreparedObjectToPush);

                console.log(i +  "  -  Продукт " + products[i].children[3].children[3].children[1].data.slice(0, -3));

                // using download function to save images
                if(productMainIMG.slice(0, 4) !== "data") {
                    download(productMainIMG, `herren/${productCategory}-${productName}.jpg`, function() { });
                }


                // getting single file information

                request(productUrl, function (error, response, body) {
                if (!error) {
                    let $ = cheerio.load(body),
                        singleProduct = $("#productinfo");
                        productTitle = $("#productTitle span").text(); 
                        productPrice = $(".price2 span").text(); 

                        // productSizes = $(".size");
                        productSizes = $(".selectSize");

                        let itemDescriptions = $("#productinfo .overview");
                        // console.log("SINGLE PRODUCT" + itemDescriptions[0]);
                        // console.dir(productTitle);

                        let completedDesription = '';

                        let productSizesInfoArray = [];

                        if(itemDescriptions[0]) {
                            completedDesription = itemDescriptions[0].children[0].data.replace(/[\n\r\t]+/g, '').replace(/\s{2,10}/g, ' ');
                        }

                        // console.log("productSizes " + productSizes.length);


                        let completedProductTitle = productTitle.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ').replace(/\//g, '-');


                        let completedProductPrice = productPrice.slice(0, -2);


                        // let completedProductSizes = productSizes[0].children[1];
                        // let completedProductSizes = productSizes;
                        // let ddddd = document.getElementsByClassName("sizes");
                        // let zzzzz = document.getElementsByClassName("selectSize");

                        for (let t = 0; t <= productSizes.length - 1; t++) {
                            let completedDataSKU = productSizes[t].attribs['data-sku']; // works
                            let completedDataSizePrice = productSizes[t].attribs['data-price']; // works
                            let completedDataID = productSizes[t].attribs.id; // works
                            let completedDataRealSize = productSizes[t].children[0].data.replace(/[\n\r\t]+/g, ''); // works

                            let sizesObjectToPush = {completedDataSKU, completedDataSizePrice, completedDataID, completedDataRealSize};
                            productSizesInfoArray.push(sizesObjectToPush);
                        }

                        // let completedProductSizes = productSizes;
                        


                        let localTimestamp = new Date().getTime();


                        let preparedProductToPush = {
                            completedDesription,
                            completedProductTitle,
                            completedProductPrice,
                            productSizesInfoArray
                        };

                        fs.writeFileSync('product-price.js', util.inspect(productSizesInfoArray) , 'utf-8');

                        fs.writeFileSync(`./products/herren/${completedProductTitle}.js`, util.inspect(preparedProductToPush) , 'utf-8');
                } else {
                    console.log("Произошла ошибка: " + error);
                }

            });

            }
            fs.writeFileSync('./data.json', util.inspect(products[0].children[1].children[1].children[1].attribs.src) , 'utf-8');

            // output file

            
            fs.writeFileSync('./preparedProducts.js', util.inspect(ArrayOfProducts) , 'utf-8');
            // console.log(util.inspect(products[0], false, null))

    } else {
        console.log("Произошла ошибка: " + error);
    }

});

    for( let i = 2; i <= 26; i++) {

        setTimeout(() => {
            console.log('CURRENT ITERATION ' + i)
         return request(localURL, function (error, response, body) {
    if (!error) {
        let $ = cheerio.load(body),
            products = $(".productData");
            for (let i = 0; i < products.length - 1; i++) {
                // console.log(util.inspect(products[0], false, null))

                // let productObject = document.getElementById("productList");
                // let productObjectS = document.getElementsByClassName("productData");

                let productPrice = '';

                let productUrl = products[i].children[1].attribs.href;
                let productCategory = products[i].children[3].children[1].children[1].children[0].data;
                let productName = products[i].children[3].children[1].children[3].children[0].data.replace(/\//g, '-');
                if (products[i].children[3].children[3].children[1].data) {
                    productPrice = products[i].children[3].children[3].children[1].data.slice(0, -3);
                }
                
                let productMainIMG = products[i].children[1].children[1].children[1].attribs.src;

                let PreparedObjectToPush = {
                    productUrl, productCategory, productName, productPrice, productMainIMG
                };


                ArrayOfProducts.push(PreparedObjectToPush);

                console.log(i +  " - Продукт " + productName + " : " + productPrice);

                // using download function to save images
                if(productMainIMG && productMainIMG.slice(0, 4) !== "data") {
                    download(productMainIMG, `herren/${productCategory}-${productName}.jpg`, function() { });
                }


                // getting single file information

                request(productUrl, function (error, response, body) {
                if (!error) {
                    let $ = cheerio.load(body),
                        singleProduct = $("#productinfo");
                        productTitle = $("#productTitle span").text(); 
                        productPrice = $(".price2 span").text(); 

                        // productSizes = $(".size");
                        productSizes = $(".selectSize");

                        let itemDescriptions = $("#productinfo .overview");
                        // console.log("SINGLE PRODUCT" + itemDescriptions[0]);
                        // console.dir(productTitle);

                        let completedDesription = '';

                        let productSizesInfoArray = [];

                        if(itemDescriptions[0]) {
                            completedDesription = itemDescriptions[0].children[0].data.replace(/[\n\r\t]+/g, '').replace(/\s{2,10}/g, ' ');
                        }

                        // console.log("productSizes " + productSizes.length);


                        let completedProductTitle = productTitle.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ').replace(/\//g, '-');


                        let completedProductPrice = productPrice.slice(0, -2);


                        // let completedProductSizes = productSizes[0].children[1];
                        // let completedProductSizes = productSizes;
                        // let ddddd = document.getElementsByClassName("sizes");
                        // let zzzzz = document.getElementsByClassName("selectSize");

                        for (let t = 0; t <= productSizes.length - 1; t++) {
                            let completedDataSKU = productSizes[t].attribs['data-sku']; // works
                            let completedDataSizePrice = productSizes[t].attribs['data-price']; // works
                            let completedDataID = productSizes[t].attribs.id; // works
                            let completedDataRealSize = productSizes[t].children[0].data.replace(/[\n\r\t]+/g, ''); // works

                            let sizesObjectToPush = {completedDataSKU, completedDataSizePrice, completedDataID, completedDataRealSize};
                            productSizesInfoArray.push(sizesObjectToPush);
                        }

                        // let completedProductSizes = productSizes;
                        


                        let localTimestamp = new Date().getTime();


                        let preparedProductToPush = {
                            completedDesription,
                            completedProductTitle,
                            completedProductPrice,
                            productSizesInfoArray
                        };

                        fs.writeFileSync('product-price.js', util.inspect(productSizesInfoArray) , 'utf-8');

                        fs.writeFileSync(`./products/herren/${completedProductTitle}.js`, util.inspect(preparedProductToPush) , 'utf-8');
                } else {
                    console.log("Произошла ошибка: " + error);
                }

            });

            }

            // fs.writeFileSync('./data.json', util.inspect(products[0].children[1].children[1].children[1].attribs.src) , 'utf-8');

            // output file

            
            // fs.writeFileSync('./preparedProducts.js', util.inspect(ArrayOfProducts) , 'utf-8');
            // console.log(util.inspect(products[0], false, null))

    } else {
        console.log("Произошла ошибка: " + error);
    }

});
     }, 3700*(i+1));
        
        let localURL = `https://www.outdoorshop.de/Bekleidung/Herren/${i}/?cl=alist&searchparam=&cnid=47`;
        // let localURL = `https://www.outdoorshop.de/Bekleidung/Damen/?ldtype=grid&_artperpage=120&pgNr=${i}&cl=alist&searchparam=&cnid=47`;

            // request multiple products page
    
    }