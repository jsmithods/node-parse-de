let request = require("request"),
  cheerio = require("cheerio"),
  fs = require("fs");
url = "https://www.outdoorshop.de/Bekleidung/Damen/";
const util = require('util');

const Sequelize = require('sequelize');
const sequelize = new Sequelize('outdoor-test', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

const Good = sequelize.define('good', {
  title: Sequelize.STRING,
  category: Sequelize.STRING,
  SKU: Sequelize.STRING,
  description: Sequelize.TEXT,
  materialDescription: Sequelize.STRING,
  price: Sequelize.STRING,
  imgUrl: Sequelize.STRING
});

const GoodMods = sequelize.define('good-mods', {
  parentSKU: Sequelize.STRING,
  dataSKU: Sequelize.STRING,
  dataSizePrice: Sequelize.STRING,
  innerId: Sequelize.STRING,
  size: Sequelize.STRING
});

const GoodSpecification = sequelize.define('good-specification', {
  parentSKU: Sequelize.STRING,
  specificationName: Sequelize.STRING,
  specificationDescription: Sequelize.STRING,
});

// function to download content
let download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(`./images/${filename}`)).on('close', callback);
  });
};

getCompletedParentSKU = function(childSKU) {
  let edge = childSKU.lastIndexOf("-");
  let start = childSKU.lastIndexOf(": ") + 2;
  return childSKU.substring(start, edge);
};

computeSKUIMG = function(SKU) {
  let start = SKU.lastIndexOf(": ") + 2;
  return SKU.substring(start);
};

filterOnlyValidColorContainer = function(value) {
  return value.name === 'li';
};

filterTableRows = function(value) {
  return value.name === 'tr';
};

getSpecificationItems = function (table, parentSKU) {
  let speciItems = [];
  let tableRowsLength = table.length;
  for(let i = 0; i < tableRowsLength; i++) {
    let specificationName = table[i].children[1].children[0].children[0].data;
    let specificationValue = table[i].children[3].children[0].data;
    let pushObject = {parentSKU, specificationName, specificationValue};
    sequelize.sync()
      .then(() => GoodSpecification.create({
        parentSKU: parentSKU,
        specificationName: specificationName,
        specificationDescription: specificationValue,
      }));
    speciItems.push(pushObject);
  }
  return speciItems;
};

let productUrl = 'https://www.outdoorshop.de/Bekleidung/Herren/Hose-nicht-wasserdicht/Makke-Pant.html';

// single product
request(productUrl, function (error, response, body) {
  if (!error) {
    let $ = cheerio.load(body);
    let productTitle = $("#productTitle span").text();
    let productPrice = $(".price2 span").text();
    let productSizes = $(".selectSize");
    let productSpecification = $(".attributes");

    let completedProductTitle = productTitle.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ').replace(/\//g, '-');

    let completedProductPrice = productPrice.slice(0, -2);

    let completedParentSKU = 'ZZZ';
    if (productSizes[0]) {
      completedParentSKU = getCompletedParentSKU(productSizes[0].attribs['data-sku']);
    }

    // colored items
    let colorCoutainer = $(".colorContainer");
    let colorVariationsCount = 1;
    let colorVariationsLinkArray = [];
    if (colorCoutainer[0]) {
      colorVariationsCount = colorCoutainer[0].children[1].children.filter(filterOnlyValidColorContainer).length;
      colorVariationsLinkArray = colorCoutainer[0].children[1].children.filter(filterOnlyValidColorContainer).map(value => {
        return value.children[1].children[1].attribs.href;
      });
    }

    // Main-image
    let mainProductImage = $(".cloud-zoom");
    let mainProductImage2 = $("#wrap");
    let selectedImageHref = mainProductImage2[0].children[1].attribs.href;
    let computedImage = mainProductImage[0].attribs.href;

    if (productSizes[0]) {
      download(selectedImageHref,`damen/${completedProductTitle}-${computeSKUIMG(productSizes[0].attribs['data-sku'])}.jpg`, function() { });
    } else {
      download(selectedImageHref,`damen/${completedProductTitle}-ZZZZZ.jpg`, function() { });
    }


    let itemDescriptions = $("#productinfo .overview");

    let completedDescription = '';

    let productSizesInfoArray = [];

    if(itemDescriptions[0]) {
      let description2 = '';
      let description1 = itemDescriptions[0].children[0].data.replace(/[\n\r\t]+/g, '').replace(/\s{2,10}/g, ' ');
      if(itemDescriptions[0].children[1] && itemDescriptions[0].children[1].children[0]) {
        description2 = itemDescriptions[0].children[1].children[0].data;
      }
      if (description1.length > description2) {
        completedDescription = description1;
      } else {
        completedDescription = description2;
      }
    }

    let materialDescription = itemDescriptions[1].children[2].data.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ');

    // let completedProductSizes = productSizes[0].children[1];
    // let completedProductSizes = productSizes;
    // let ddddd = document.getElementsByClassName("sizes");
    // let zzzzz = document.getElementsByClassName("selectSize");

    for (let t = 0; t <= productSizes.length - 1; t++) {
      let completedDataSKU = productSizes[t].attribs['data-sku']; // works
      let completedDataSizePrice = productSizes[t].attribs['data-price']; // works
      let completedDataID = productSizes[t].attribs.id; // works
      let completedDataRealSize = productSizes[t].children[0].data.replace(/[\n\r\t]+/g, ''); // works

      let sizesObjectToPush = {completedDataSKU, completedDataSizePrice, completedDataID, completedDataRealSize};
      productSizesInfoArray.push(sizesObjectToPush);
    }

    let preparedProductToPush = {
      completedDescription,
      completedParentSKU,
      completedProductTitle,
      completedProductPrice,
      materialDescription,
      productSizesInfoArray,
      selectedImageHref
    };

    if(productSizesInfoArray.length > 0) {

      sequelize.sync()
        .then(() => Good.create({
          title: preparedProductToPush.completedProductTitle,
          category: 'Damen',
          SKU: preparedProductToPush.completedParentSKU,
          description: preparedProductToPush.completedDescription,
          materialDescription: preparedProductToPush.materialDescription,
          price: preparedProductToPush.completedProductPrice,
          imgUrl: preparedProductToPush.selectedImageHref
        }));

      // Add Parent Good Info
      for (let itemSQL = 0;itemSQL <= productSizesInfoArray.length; itemSQL++) {
        let currentItem = productSizesInfoArray[itemSQL];

        // Create Product Single Mod
        sequelize.sync()
          .then(() => GoodMods.create({
            parentSKU: preparedProductToPush.completedParentSKU,
            dataSKU: currentItem.completedDataSKU,
            dataSizePrice: currentItem.completedDataSizePrice,
            innerId: currentItem.completedDataID,
            size: currentItem.completedDataRealSize
          }));

      }
    } else {
      sequelize.sync()
        .then(() => Good.create({
          title: preparedProductToPush.completedProductTitle,
          category: 'Damen',
          description: preparedProductToPush.completedDesription,
          price: preparedProductToPush.completedProductPrice
        }));
    }

    let spicificationItemsTablePush = getSpecificationItems(productSpecification[0].children[1].children.filter(filterTableRows), completedParentSKU);


    // PUSH TO FILE
    fs.writeFileSync('product-price.js', util.inspect(productSpecification[0].children[1].children.filter(filterTableRows)[0].children[3].children[0].data) , 'utf-8');

    fs.writeFileSync('product-specification.js', util.inspect(spicificationItemsTablePush) , 'utf-8');

    if (colorVariationsCount > 1) {
      for (let colorItem = 1; colorItem < colorVariationsCount; colorItem++) {

        let localColorModUrl = colorVariationsLinkArray[colorItem];
        setTimeout(() => {
          console.log('CURRENT ITEM ITERATION ' + colorItem);
          return request(localColorModUrl, function (error, response, body) {
            if (!error) {
              let $ = cheerio.load(body),
                productTitle = $("#productTitle span").text();
              productPrice = $(".price2 span").text();
              productSizes = $(".selectSize");


              let itemDescriptions = $("#productinfo .overview");

              let completedDescription = '';

              let productSizesInfoArray = [];

              if(itemDescriptions[0]) {
                let description2 = '';
                let description1 = itemDescriptions[0].children[0].data.replace(/[\n\r\t]+/g, '').replace(/\s{2,10}/g, ' ');
                if(itemDescriptions[0].children[1] && itemDescriptions[0].children[1].children[0]) {
                  description2 = itemDescriptions[0].children[1].children[0].data;
                }
                if (description1.length > description2) {
                  completedDescription = description1;
                } else {
                  completedDescription = description2;
                }
              }

              // console.log("productSizes " + productSizes.length);

              let completedProductTitle = productTitle.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ').replace(/\//g, '-');

              let completedProductPrice = productPrice.slice(0, -2);

              let materialDescription = itemDescriptions[1].children[2].data.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ');

              // let completedProductSizes = productSizes[0].children[1];
              // let completedProductSizes = productSizes;
              // let ddddd = document.getElementsByClassName("sizes");
              // let zzzzz = document.getElementsByClassName("selectSize");

              let completedParentSKU = 'ZZZZ';
              if (productSizes[0]) {
                completedParentSKU = getCompletedParentSKU(productSizes[0].attribs['data-sku']);
              }


              // Main-image
              let mainProductImage = $(".cloud-zoom");
              let computedImage = '1';
              if (mainProductImage[0]) {
                computedImage = mainProductImage[0].attribs.href;
              }


              if (productSizes[0]) {
                download(computedImage,`damen/${completedProductTitle}-${computeSKUIMG(productSizes[0].attribs['data-sku'])}.jpg`, function() { });
              } else {
                download(computedImage,`damen/${completedProductTitle}-ZZZZZ.jpg`, function() { });
              }

              for (let t = 0; t <= productSizes.length - 1; t++) {
                let completedDataSKU = productSizes[t].attribs['data-sku']; // works
                let completedDataSizePrice = productSizes[t].attribs['data-price']; // works
                let completedDataID = productSizes[t].attribs.id; // works
                let completedDataRealSize = productSizes[t].children[0].data.replace(/[\n\r\t]+/g, ''); // works

                let sizesObjectToPush = {completedDataSKU, completedDataSizePrice, completedDataID, completedDataRealSize};
                productSizesInfoArray.push(sizesObjectToPush);
              }

              // let completedProductSizes = productSizes;

              // let localTimestamp = new Date().getTime();

              let preparedProductToPush = {
                completedDescription,
                completedParentSKU,
                completedProductTitle,
                completedProductPrice,
                materialDescription,
                productSizesInfoArray
              };

              // fs.writeFileSync('product-price.js', util.inspect(materialDescription) , 'utf-8');

              if(productSizesInfoArray.length > 0) {

                sequelize.sync()
                  .then(() => Good.create({
                    title: preparedProductToPush.completedProductTitle,
                    category: 'Damen',
                    SKU: preparedProductToPush.completedParentSKU,
                    description: preparedProductToPush.completedDescription,
                    materialDescription: preparedProductToPush.materialDescription,
                    price: preparedProductToPush.completedProductPrice,
                    imgUrl:computedImage
                  }));

                // Add Parent Good Info
                for (let itemSQL = 0;itemSQL <= productSizesInfoArray.length; itemSQL++) {
                  let currentItem = productSizesInfoArray[itemSQL];

                  // Create Product Single Mod
                  sequelize.sync()
                    .then(() => GoodMods.create({
                      parentSKU: preparedProductToPush.completedParentSKU,
                      dataSKU: currentItem.completedDataSKU,
                      dataSizePrice: currentItem.completedDataSizePrice,
                      innerId: currentItem.completedDataID,
                      size: currentItem.completedDataRealSize
                    }));

                }
              } else {
                sequelize.sync()
                  .then(() => Good.create({
                    title: preparedProductToPush.completedProductTitle,
                    category: 'Damen',
                    description: preparedProductToPush.completedDesription,
                    price: preparedProductToPush.completedProductPrice
                  }));
              }

            } else {
              console.log("Произошла ошибка: " + error);
            }
          });
        }, 110*(colorItem+1));
      }
    }

    // fs.writeFileSync('product-price.js', util.inspect(materialDescription) , 'utf-8');

    // fs.writeFileSync('product-description.js', util.inspect(colorVariationsLinkArray) , 'utf-8');

    // fs.writeFileSync(`./products/damen/${completedProductTitle}.js`, util.inspect(preparedProductToPush) , 'utf-8');
  } else {
    console.log("Произошла ошибка: " + error);
  }
});
