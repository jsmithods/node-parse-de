let request = require("request"),
  cheerio = require("cheerio"),
  fs = require("fs");
  url = "https://www.outdoorshop.de/Bekleidung/Damen/";
  // const util = require('util');

const pagesStruct = {
  kleidungDamen: {
    baseUrl: 'https://www.outdoorshop.de/Bekleidung/Damen/?cl=alist&searchparam=&cnid=47',
    nextUrl: 'https://www.outdoorshop.de/Bekleidung/Damen/2/?cl=alist&searchparam=&cnid=47'
  },
  kleidungHerren: {
    baseUrl: 'https://www.outdoorshop.de/Bekleidung/Herren/?cl=alist&searchparam=&cnid=49',
    nextUrl: 'https://www.outdoorshop.de/Bekleidung/Herren/2/?cl=alist&searchparam=&cnid=49'
  },
  shuheDamen: {
    baseUrl: 'https://www.outdoorshop.de/Schuhe/Damen/?cl=alist&searchparam=&cnid=163',
    nextUrl: 'https://www.outdoorshop.de/Schuhe/Damen/2/?cl=alist&searchparam=&cnid=163'
  },
  shuheHerren: {
    baseUrl: 'https://www.outdoorshop.de/Schuhe/Herren/?cl=alist&searchparam=&cnid=213',
    nextUrl: 'https://www.outdoorshop.de/Schuhe/Herren/2/?cl=alist&searchparam=&cnid=213'
  },
  bergSport: {
    baseUrl: 'https://www.outdoorshop.de/Bergsport/?cl=alist&searchparam=&cnid=2',
    nextUrl: 'https://www.outdoorshop.de/Bergsport/2/?cl=alist&searchparam=&cnid=2'
  },
  ausrustung: {
    baseUrl: 'https://www.outdoorshop.de/Ausruestung/?cl=alist&searchparam=&cnid=11',
    nextUrl: 'https://www.outdoorshop.de/Ausruestung/2/?cl=alist&searchparam=&cnid=11'
  },
  reisezubehor: {
    baseUrl: 'https://www.outdoorshop.de/Reisezubehoer/?cl=alist&searchparam=&cnid=29',
    nextUrl: 'https://www.outdoorshop.de/Reisezubehoer/2/?cl=alist&searchparam=&cnid=29'
  },
  ruckzack: {
    baseUrl: 'https://www.outdoorshop.de/Rucksack/?cl=alist&searchparam=&cnid=7',
    nextUrl: 'https://www.outdoorshop.de/Rucksack/2/?cl=alist&searchparam=&cnid=7'
  },
  schlafen: {
    baseUrl: 'https://www.outdoorshop.de/Schlafen/?cl=alist&searchparam=&cnid=39',
    nextUrl: 'https://www.outdoorshop.de/Schlafen/2/?cl=alist&searchparam=&cnid=39'
  },
  zelt: {
    baseUrl: 'https://www.outdoorshop.de/Zelt/?cl=alist&searchparam=&cnid=65',
    nextUrl: 'https://www.outdoorshop.de/Zelt/2/?cl=alist&searchparam=&cnid=65'
  },
  kinder: {
    baseUrl: 'https://www.outdoorshop.de/Kinder/?cl=alist&searchparam=&cnid=37',
    nextUrl: 'https://www.outdoorshop.de/Kinder/2/?cl=alist&searchparam=&cnid=37'
  },
};

const baseDirectory = 'prod-images';
  const Sequelize = require('sequelize');
  const sequelize = new Sequelize('outdoor-shop-2', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: false,

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  });

const Good = sequelize.define('good', {
  title: Sequelize.STRING,
  category: Sequelize.STRING,
  SKU: Sequelize.STRING,
  description: Sequelize.TEXT,
  materialDescription: Sequelize.STRING,
  price: Sequelize.STRING,
  imgUrl: Sequelize.STRING
});

const GoodMods = sequelize.define('good-mods', {
  parentSKU: Sequelize.STRING,
  dataSKU: Sequelize.STRING,
  dataSizePrice: Sequelize.STRING,
  innerId: Sequelize.STRING,
  size: Sequelize.STRING
});

const GoodSpecification = sequelize.define('good-specification', {
  parentSKU: Sequelize.STRING,
  specificationName: Sequelize.STRING,
  specificationDescription: Sequelize.STRING,
});

// Create folder if not exists

checkCreateFolder = function (folderName) {
  if (!fs.existsSync(folderName)){
    fs.mkdirSync(folderName);
  }
};

// function to download content
let download = function(uri, dirname, filename, callback){
  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(`${dirname}/${filename}`)).on('close', callback);
  });
};

getCompletedParentSKU = function(childSKU) {
  let edge = childSKU.lastIndexOf("-");
  let start = childSKU.lastIndexOf(": ") + 2;
  return childSKU.substring(start, edge);
};

computeSKUIMG = function(SKU) {
  let start = SKU.lastIndexOf(": ") + 2;
  return SKU.substring(start).replace(/\//g, '-');
};

filterOnlyValidColorContainer = function(value) {
  return value.name === 'li';
};

filterTableRows = function(value) {
  return value.name === 'tr';
};

getSpecificationItems = function (table, parentSKU) {
  let speciItems = [];
  let tableRowsLength = table.length;
  for(let i = 0; i < tableRowsLength; i++) {
    let specificationName = table[i].children[1].children[0].children[0].data;
    let specificationValue = table[i].children[3].children[0].data;
    let pushObject = {parentSKU, specificationName, specificationValue};
    sequelize.sync()
      .then(() => GoodSpecification.create({
        parentSKU: parentSKU,
        specificationName: specificationName,
        specificationDescription: specificationValue,
      }));
    speciItems.push(pushObject);
  }
  return speciItems;
};

let ArrayOfProducts = [];

iterateCategory = function (
  iterationCount,
  firstItemURL,
  urlPrefix,
  categorySeries
  ) {
  for(let i = 1; i <= iterationCount; i++) {
    let localURL = '';
    if(i === 1) {
      localURL = `${firstItemURL}/${urlPrefix}`;
      console.log(localURL);
    } else {
      localURL = `${firstItemURL}/${i}/${urlPrefix}`;
      console.log(localURL);
    }

    setTimeout(() => {
      console.log('CURRENT ITERATION ' + i);
      return request(localURL, function (error, response, body) {
        if (!error) {
          let $ = cheerio.load(body),
            products = $(".productData");

          checkCreateFolder(baseDirectory);

          checkCreateFolder(`${baseDirectory}/damen`);
          for (let i = 0; i < products.length - 1; i++) {

            let productPrice = '';

            let productUrl = products[i].children[1].attribs.href;
            let productCategory = products[i].children[3].children[1].children[1].children[0].data;
            let productName = products[i].children[3].children[1].children[3].children[0].data.replace(/\//g, '-');
            if (products[i].children[3].children[3].children[1].data) {
              productPrice = products[i].children[3].children[3].children[1].data.slice(0, -3);
            }

            let productMainIMG = products[i].children[1].children[1].children[1].attribs.src;

            let PreparedObjectToPush = {
              productUrl, productCategory, productName, productPrice, productMainIMG
            };

            ArrayOfProducts.push(PreparedObjectToPush);

            console.log(i +  " - Продукт " + productName + " : " + productPrice);

            // using download function to save images
            /*if(productMainIMG && productMainIMG.slice(0, 4) !== "data") {
                download(productMainIMG, `damen/${productCategory}-${productName}.jpg`, function() { });
            }*/

            // getting single file information
            request(productUrl, function (error, response, body) {
              if (!error) {
                let $ = cheerio.load(body);
                let productTitle = $("#productTitle span").text();
                let productPrice = $(".price2 span").text();
                let productSizes = $(".selectSize");
                let productSpecification = $(".attributes");

                let completedProductTitle = productTitle.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ').replace(/\s/g, '');

                let completedProductPrice = productPrice.slice(0, -2);

                let completedParentSKU = 'ZZZ';
                if (productSizes[0]) {
                  completedParentSKU = getCompletedParentSKU(productSizes[0].attribs['data-sku']);
                }

                // colored items
                let colorCoutainer = $(".colorContainer");
                let colorVariationsCount = 1;
                let colorVariationsLinkArray = [];
                if (colorCoutainer[0]) {
                  colorVariationsCount = colorCoutainer[0].children[1].children.filter(filterOnlyValidColorContainer).length;
                  colorVariationsLinkArray = colorCoutainer[0].children[1].children.filter(filterOnlyValidColorContainer).map(value => {
                    return value.children[1].children[1].attribs.href;
                  });
                }


                // Main-image
                let mainProductImage = $(".cloud-zoom");
                let mainProductImage2 = $("#wrap");
                let selectedImageHref = mainProductImage2[0].children[1].attribs.href;
                let computedImage = mainProductImage[0].attribs.href;

                if (productSizes[0]) {
                  // download(selectedImageHref,'damen',`${completedProductTitle}-${computeSKUIMG(productSizes[0].attribs['data-sku'])}.jpg`, function() { });
                } else {
                  // download(selectedImageHref,'damen/', `${completedProductTitle}-ZZZZZ.jpg`, function() { });
                }

                let itemDescriptions = $("#productinfo .overview");

                let completedDescription = '';

                let productSizesInfoArray = [];

                if(itemDescriptions[0]) {
                  let description2 = '';
                  let description1 = itemDescriptions[0].children[0].data.replace(/[\n\r\t]+/g, '').replace(/\s{2,10}/g, ' ');
                  if(itemDescriptions[0].children[1] && itemDescriptions[0].children[1].children[0]) {
                    description2 = itemDescriptions[0].children[1].children[0].data;
                  }
                  if (description1.length > description2) {
                    completedDescription = description1;
                  } else {
                    completedDescription = description2;
                  }
                }

                let materialDescription = '';
                if (itemDescriptions[1].children[2].data) {
                  materialDescription = itemDescriptions[1].children[2].data.replace(/\s{2,10}/g, ' ').replace(/\s{2,10}/g, ' ');
                }

                // let completedProductSizes = productSizes[0].children[1];
                // let completedProductSizes = productSizes;
                // let ddddd = document.getElementsByClassName("sizes");
                // let zzzzz = document.getElementsByClassName("selectSize");

                for (let t = 0; t <= productSizes.length - 1; t++) {
                  let completedDataSKU = productSizes[t].attribs['data-sku']; // works
                  let completedDataSizePrice = productSizes[t].attribs['data-price']; // works
                  let completedDataID = productSizes[t].attribs.id; // works
                  let completedDataRealSize = productSizes[t].children[0].data.replace(/[\n\r\t]+/g, ''); // works

                  let sizesObjectToPush = {completedDataSKU, completedDataSizePrice, completedDataID, completedDataRealSize};
                  productSizesInfoArray.push(sizesObjectToPush);
                }

                let preparedProductToPush = {
                  completedDescription,
                  completedParentSKU,
                  completedProductTitle,
                  completedProductPrice,
                  materialDescription,
                  productSizesInfoArray,
                  selectedImageHref
                };

                if(productSizesInfoArray.length > 0) {

                  sequelize.sync()
                    .then(() => Good.create({
                      title: preparedProductToPush.completedProductTitle,
                      category: categorySeries,
                      SKU: preparedProductToPush.completedParentSKU,
                      description: preparedProductToPush.completedDescription,
                      materialDescription: preparedProductToPush.materialDescription,
                      price: preparedProductToPush.completedProductPrice,
                      imgUrl: preparedProductToPush.selectedImageHref
                    }));

                  // Add Parent Good Info
                  for (let itemSQL = 0;itemSQL <= productSizesInfoArray.length; itemSQL++) {
                    let currentItem = productSizesInfoArray[itemSQL];

                    // Create Product Single Mod
                    sequelize.sync()
                      .then(() => GoodMods.create({
                        parentSKU: preparedProductToPush.completedParentSKU,
                        dataSKU: currentItem.completedDataSKU,
                        dataSizePrice: currentItem.completedDataSizePrice,
                        innerId: currentItem.completedDataID,
                        size: currentItem.completedDataRealSize
                      }));

                  }
                } else {
                  sequelize.sync()
                    .then(() => Good.create({
                      title: preparedProductToPush.completedProductTitle,
                      category: categorySeries,
                      description: preparedProductToPush.completedDesription,
                      price: preparedProductToPush.completedProductPrice
                    }));
                }

                let spicificationItemsTablePush = '';
                if (productSpecification[0]) {
                  spicificationItemsTablePush = getSpecificationItems(productSpecification[0].children[1].children.filter(filterTableRows), completedParentSKU);
                }

                console.log(spicificationItemsTablePush);


                // PUSH TO FILE
                // fs.writeFileSync('product-price.js', util.inspect(productSpecification[0].children[1].children.filter(filterTableRows)[0].children[3].children[0].data) , 'utf-8');

                // fs.writeFileSync('product-specification.js', util.inspect(spicificationItemsTablePush) , 'utf-8');

                // fs.writeFileSync('product-price.js', util.inspect(materialDescription) , 'utf-8');

                // fs.writeFileSync('product-description.js', util.inspect(colorVariationsLinkArray) , 'utf-8');

                // fs.writeFileSync(`./products/damen/${completedProductTitle}.js`, util.inspect(preparedProductToPush) , 'utf-8');
              } else {
                console.log("Произошла ошибка: " + error);
              }
            });
          }

          // fs.writeFileSync('./data.json', util.inspect(products[0].children[1].children[1].children[1].attribs.src) , 'utf-8');

          // output file

          // fs.writeFileSync('./preparedProducts.js', util.inspect(ArrayOfProducts) , 'utf-8');
          // console.log(util.inspect(products[0], false, null))

        } else {
          console.log("Произошла ошибка: " + error);
        }
      });
    }, 5000*(i));

  }
};



iterateCategory(
  20,
  'https://www.outdoorshop.de/Bekleidung/Damen',
  '?cl=alist&searchparam=&cnid=47',
  'Bekleidung-Damen'
  );

setTimeout(() => {
  iterateCategory(
    20,
    'https://www.outdoorshop.de/Bekleidung/Herren',
    '?cl=alist&searchparam=&cnid=49',
    'Bekleidung-Herren'
  );
}, 10000);
